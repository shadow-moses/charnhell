﻿using UnityEngine;

public class EnemyFollow : MonoBehaviour {
    public GameObject target = null;
    public AudioClip[] AttackSounds;

    private DamageHandler targetDamageHandler;
    private Rigidbody2D rigidBody;
    private Animator animator;
    private DamageHandler damageHandler;

    public int state = (int)EnemyState.Spawning;
    public float speed = 5f;
    public float aggroRadius = 0.5f;
    public float interestRadius = 1f;
    public float attackStrength = 10f;
    public float attackRange = 0.01f;
    public Vector2 wanderPosition;

    private GameObject[] _players;

    private void Awake() {
        _players = GameManager.Instance.AllPlayers;
        animator = GetComponent<Animator>();
        damageHandler = GetComponent<DamageHandler>();
        rigidBody = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        state = (int)EnemyState.Spawning;
    }

    public void Spawned()
    {
        state = (int)EnemyState.Idle;
    }

    private void FixedUpdate() {
        if (state == (int)EnemyState.Spawning)
        {
            return;
        }
        if (state == (int)EnemyState.Wandering)
        {
            if (Random.value < 0.005f)
            {
                state = (int)EnemyState.Idle;
                wanderPosition = SpawnHelper.GetArenaSpawnLocation(GameManager.Instance.SpawnExtents);
                return;
            }
            MoveTowardsPoint();
        }
        // Update velocity for animation
        animator.SetFloat(AnimationSettings.EnemyVelocity, rigidBody.velocity.magnitude);

        if (state == (int)EnemyState.Chasing && target)
        {
            // If our target is in range, attack
            if (state != (int)EnemyState.Attacking && Vector2.Distance(target.transform.position, transform.position) < attackRange)
            {
                if (targetDamageHandler)
                Attack();
                return;
            }
            else
            {
                // If our target is outside our area of interest, become idle again
                if (Vector2.Distance(target.transform.position, transform.position) > interestRadius)
                {
                    target = null;
                    state = (int)EnemyState.Idle;
                    return;
                }
                // Otherwise give chase
                else
                {
                    MoveTowardsTarget();
                    return;
                }
            }
        }
        else {
            if (state == (int)EnemyState.Idle || state == (int)EnemyState.Wandering)
            {
                if (Random.value < 0.005f)
                {
                    state = (int)EnemyState.Wandering;
                    return;
                }
                target = ProximityHelper.FindNearest(transform.position, aggroRadius, _players);
                if (target != null)
                {
                    state = (int)EnemyState.Chasing;
                    targetDamageHandler = target.GetComponent<DamageHandler>();
                }
                return;
            }
        }
    }

    //public void Update()
    //{
    //    if (Input.GetKeyDown("space"))
    //    {
    //        damageHandler.Damage(Random.Range(1f, 10f));
    //    }
    //}

    private void MoveTowardsTarget() {
        float localSpeed = (transform.position - target.transform.position).sqrMagnitude < aggroRadius ? speed * 2f : speed;

        rigidBody.AddForce((target.transform.position - transform.position).normalized * localSpeed);
    }

    private void MoveTowardsPoint()
    {
        rigidBody.AddForce(((Vector3)wanderPosition - transform.position).normalized * (speed * 0.5f));
    }

    private void Attack()
    {
        if (state != (int)EnemyState.Attacking) {
            state = (int)EnemyState.Attacking;
            animator.Play("skeleton_attack_s");
            AudioManager.Instance.PlayAudioClip(AudioPool.Enemy, AttackSounds);
        }
    }

    private void Lunge()
    {
        rigidBody.AddForce((target.transform.position - transform.position).normalized * (speed * 10f));
    }

    private void DoDamage()
    {
        if (target && Vector2.Distance(target.transform.position, transform.position) < attackRange)
        {
            targetDamageHandler.Damage(attackStrength);
        }
    }

    private void EndAttack()
    {
        state = (int)EnemyState.Chasing;
    }

    //public void OnDrawGizmosSelected()
    //{
    //    // Attack range
    //    UnityEditor.Handles.color = new Color(1f, 0f, 0f);
    //    UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, attackRange);

        
    //    // Aggro radius
    //    UnityEditor.Handles.color = new Color(1f, 0.5f, 0f);
    //    UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, aggroRadius);

    //    // Interest range
    //    UnityEditor.Handles.color = new Color(0.5f, 0.5f, 0.5f);
    //    UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.forward, interestRadius);
        
    //}
}