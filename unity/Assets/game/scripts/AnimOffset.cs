﻿using UnityEngine;
using System.Collections;

public class AnimOffset : MonoBehaviour {

    private Animator animator;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();

        animator.Play(0, 0, Random.Range(0f, 1f));
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
