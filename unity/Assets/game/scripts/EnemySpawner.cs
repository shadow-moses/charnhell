﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {

    private BoxCollider2D extents;

    private float nextSpawn;
    public GameObject objectToSpawn;

    public float spawnInterval = 5f; // How often an enemy is spawned
    public float spawnIntervalVariance = 2.5f; // The random variation between each spawn
    public uint maxEnemies = 10;
    private List<GameObject> enemies = new List<GameObject>();

    public bool spawning = false;

    private void Awake()
    {
        extents = GetComponent<BoxCollider2D>();
    }
	
	// Update is called once per frame
	private void Update () {
        Spawn();
	}

    public void Spawn()
    {
        if (!objectToSpawn || !spawning)
        {
            return;
        }
        if (Time.fixedTime >= nextSpawn)
        {
            nextSpawn = Mathf.Max(0.1f, Time.fixedTime + spawnInterval + (Random.Range(spawnIntervalVariance * -1, spawnIntervalVariance)));
            if (enemies.Count >= maxEnemies)
            {
                enemies.RemoveAll(item => item == null);
            }
            else
            {
                Vector2 spawnPosition = SpawnHelper.GetArenaSpawnLocation(extents);
                GameObject newEnemy = Instantiate(objectToSpawn);
                newEnemy.transform.position = spawnPosition;
                enemies.Add(newEnemy);
                //GameManager.Instance.ShakeCamera(0.02f, 0.5f);
            }
        }
        
    }

    public void ToggleEnemies(bool active) {
        foreach (var e in enemies) {
            e.SetActive(active);
        }
    }

    public void DestroyEnemies() {
        foreach (var e in enemies) {
            Destroy(e);
        }
        enemies.Clear();
    }
}
