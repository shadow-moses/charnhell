﻿public enum EnemyState {
    Spawning,
    Idle,
    Wandering,
    Chasing,
    Attacking,
};