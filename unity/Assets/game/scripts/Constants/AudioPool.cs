﻿public enum AudioPool {
    Music = 0,
    Ambient = 1,
    Player1 = 2,
    Player2 = 3,
    Enemy = 4,
    Announcer = 5
}