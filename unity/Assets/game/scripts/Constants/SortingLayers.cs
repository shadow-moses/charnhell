﻿public static class SortingLayers {
    public static string Default = "Default";
    public static string Background = "Background";
    public static string BackgroundObjects = "BackgroundObjects";
    public static string Sorted = "Sorted";
    public static string Foreground = "Foreground";
}