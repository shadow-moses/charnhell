﻿public static class AnimationSettings {
	public static string MecanimMoving = "moving";
	public static string MecanimNorth = "north";
	public static string MecanimSouth = "south";
	public static string MecanimEast = "east";
	public static string MecanimWest = "west";

    public static string MecanimGrapple = "grapple";
    public static string MecanimGrappleQuick = "grapplequick";
    public static string MecanimDazed = "dazed";
    public static string MecanimHurt = "hurt";

    public static string EnemyVelocity = "enemyvelocity";
    public static string EnemyAttacking = "enemyattacking";
}