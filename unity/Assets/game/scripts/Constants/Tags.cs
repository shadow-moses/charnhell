﻿public static class Tags {
    public static string Player = "Player";
    public static string Enemy = "Enemy";
    public static string WeaponItem = "WeaponItem";
    public static string MoveableObject = "MoveableObject";
}