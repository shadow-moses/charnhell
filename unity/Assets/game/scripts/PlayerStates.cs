﻿public enum PlayerStates {
    /// <summary>
    /// Moving or idle
    /// </summary>
    Default,
    /// <summary>
    /// Grappling
    /// </summary>
    Grappling,
    /// <summary>
    /// Holding a weapon or swinging
    /// </summary>
    HoldingWeapon,
    /// <summary>
    /// Player is dazed
    /// </summary>
    Dazed,
    /// <summary>
    /// Player is dead
    /// </summary>
    Dead
}