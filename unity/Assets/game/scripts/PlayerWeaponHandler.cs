﻿using UnityEngine;

/// <summary>
///     Handles picking up and swinging weapons
/// </summary>
[RequireComponent(typeof (PlayerConfig))]
public class PlayerWeaponHandler : MonoBehaviour {
    public WeaponConfig HeldWeapon { get; private set; }
    public float WeaponDistanceFromPlayer = 0.2f;

    public float MaxThrowForce = 400f;
    public float MinThrowHoldTime = .5f;
    public float MaxThrowHoldTime = 2f;

    private float timeThrowHeld;
    private PlayerConfig _config;

    public void Awake() {
        _config = GetComponent<PlayerConfig>();
    }

    private void FixedUpdate() {
        // Get the buttons that are being pressed
        var buttonName = InputHelper.GetPlayerControl(_config.PlayerNumber, InputSettings.ActionButton);
        var actionUp = Input.GetButtonUp(buttonName);
        var actionHeld = Input.GetButton(buttonName);

        UpdatePickupItem(actionUp);

        UpdateWeaponDirection();

        UpdateThrowHeldWeapon(actionUp, actionHeld);
        //UpdateSwingHeldWeapon(attackDown);
    }

    private void Update() {
        if (weaponThrown) {
            // Reset the player's state to default
            _config.PlayerStateManager.TrySetState(PlayerStates.Default);

            ReleaseWeapon();

            weaponThrown = false;
        }
    }

    private void UpdateWeaponDirection() {
        // Update the weapons location based on the player's facing direction
        if (_config.PlayerStateManager.CurrentState == PlayerStates.HoldingWeapon 
            && HeldWeapon != null) {
            // Pick the weapon up
            HeldWeapon.transform.parent = transform;

            // Move the weapon to be just slightly away from the player's collider
            HeldWeapon.transform.position = _config.MovementCollider.bounds.center + (_config.PlayerMovement.LastDirection*WeaponDistanceFromPlayer);
        }
    }

    #region Pick up

    private void UpdatePickupItem(bool actionButtonUp) {
        if (!actionButtonUp) {
            // Can only pick up weapons when a button is released
            return;
        }

        // Check if we can enter "holding"
        if (!_config.PlayerStateManager.TryState(PlayerStates.HoldingWeapon)) {
            return;
        }

        var nearestWeapon = ProximityHelper.FindNearest(transform.position, .03f, GameManager.Instance.AllWeapons);
        if (nearestWeapon == null) {
            // No weapon close
            return;
        }

        // Pick the weapon up
        Debug.Log(_config.PlayerNumber + " picking up item " + nearestWeapon.name);

        // Get the weapon
        HeldWeapon = nearestWeapon.GetComponent<WeaponConfig>();

        // Update the weapon to the "held" state
        HeldWeapon.SetState(WeaponConfig.WeaponStates.BeingHeld);

        // Play the weapon's equip sound
        AudioManager.Instance.PlayAudioClip(AudioPool.Ambient, HeldWeapon.PickupClip);

        // Set the state to holding
        _config.PlayerStateManager.TrySetState(PlayerStates.HoldingWeapon);
    }

    #endregion

    #region Throw weapon

    private void UpdateThrowHeldWeapon(bool actionUp, bool actionHeld) {
        if (_config.PlayerStateManager.CurrentState != PlayerStates.HoldingWeapon) {
            ResetThrowTimer();
            return;
        }

        UpdateThrow(actionUp, actionHeld, _config.PlayerMovement.LastDirection);
    }

    private void UpdateThrow(bool actionUp, bool actionHeld, Vector2 throwDirection) {
        if (actionHeld) {
            // Action held, increment timer
            timeThrowHeld += Time.deltaTime;
            Debug.Log("Charging throw...");
        } else if (actionUp && timeThrowHeld > 0) {
            // Action released, throw based on delta + force
            Debug.Log("Chucking weapon...");
            PerformThrow(throwDirection);
        }
    }

    private bool weaponThrown;

    private void ResetThrowTimer() {
        timeThrowHeld = 0f;
    }

    private void PerformThrow(Vector2 throwDirection) {

        DisconnectWeaponFromPlayer();

        // Apply the throw force in the specified direction
        var force = MaxThrowForce * Mathf.Clamp(timeThrowHeld, MinThrowHoldTime, MaxThrowHoldTime);
        Debug.Log(_config.PlayerNumber + " throwing " + HeldWeapon.name + " with " + force + " force");
        HeldWeapon.WeaponRigidbody.AddForce(throwDirection.normalized * force);

        ResetThrowTimer();

        weaponThrown = true;
    }

    #endregion

    private void DisconnectWeaponFromPlayer() {
        // Release from player's grasp
        HeldWeapon.transform.parent = null;

        // Reset weapon's state
        //HeldWeapon.SetState(WeaponConfig.WeaponStates.OnGroundStatic);
        HeldWeapon.SetState(WeaponConfig.WeaponStates.InAir);
    }

    /// <summary>
    ///     Drops the currently held weapon. DO NOT set any player states in this method.
    /// </summary>
    public void ReleaseWeapon() {
        if (_config.PlayerStateManager.CurrentState != PlayerStates.HoldingWeapon) {
            return;
        }

        if (HeldWeapon == null) {
            Debug.LogError(_config.PlayerNumber + " state was set to HoldingWeapon, but no weapon was detected");
            return;
        }

        DisconnectWeaponFromPlayer();

        ResetThrowTimer();

        // De-register the weapon (important for state checks in DamageHandler)
        HeldWeapon = null;
    }
}