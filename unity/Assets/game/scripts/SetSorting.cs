﻿using UnityEngine;

/// <summary>
///     Sorts the order of this sprite renderer depending on the Y axis position of the object
/// </summary>
[ExecuteInEditMode]
public class SetSorting : MonoBehaviour {
    /// <summary>
    ///     If true, does not set sorting every frame
    /// </summary>
    public bool Passive = false;

    private Renderer spriteRenderer;

    public void Awake() {
        spriteRenderer = GetComponent<Renderer>();
    }

    public void Start() {
        Sort();
    }

    public void Update() {
        if (!Passive) {
            Sort();
        }
#if UNITY_EDITOR
        Sort();
#endif
    }

    public void Sort() {
        // Perform simple depth sorting on y position
        spriteRenderer.sortingLayerName = SortingLayers.Sorted;
        spriteRenderer.sortingOrder = Mathf.RoundToInt(transform.position.y*-100);
    }
}