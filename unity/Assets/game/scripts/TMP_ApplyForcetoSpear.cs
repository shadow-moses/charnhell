﻿using UnityEngine;

public class TMP_ApplyForcetoSpear : MonoBehaviour {

    public GameObject weapon;

    public void ApplySmallForce() {
        var config = weapon.GetComponent<WeaponConfig>();
        config.SetState(WeaponConfig.WeaponStates.InAir);
        weapon.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * 300f);
    }

    public void ApplyLargeForce() {
        var config = weapon.GetComponent<WeaponConfig>();
        config.SetState(WeaponConfig.WeaponStates.InAir);

        var rb = weapon.GetComponent<Rigidbody2D>();
        rb.AddRelativeForce(Vector2.right*600f);
    }
}