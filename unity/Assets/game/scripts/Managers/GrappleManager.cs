﻿using System.Collections;

using UnityEngine;

/// <summary>
///     Manages a grapple between two players
/// </summary>
public class GrappleManager : MonoBehaviour {
    #region Singleton

    private static GrappleManager _instance;

    public static GrappleManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<GrappleManager>() ?? new GameObject("GrappleManager").AddComponent<GrappleManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    private void Awake() {
        if (_instance == null) {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        } else {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance) {
                Destroy(gameObject);
            }
        }
    }

    #endregion

    /// <summary>
    ///     The number of grapple presses needed by a player before they win a grapple
    /// </summary>
    public const int GrapplePressesNeeded = 15;

    /// <summary>
    /// The amount of seconds the player that loses the grapple is dazed for
    /// </summary>
    public const float GrappleLoserDazeSeconds = 3f;

    /// <summary>
    ///     When true, the players are grappling
    /// </summary>
    public bool GrappleInitiated { get; private set; }

    /// <summary>
    ///     When true, a player has won the grapple
    /// </summary>
    public bool GrappleComplete { get; private set; }

    /// <summary>
    ///     Called once, by the player that won the grapple. Executes the throw state.
    /// </summary>
    /// <param name="playerWon"></param>
    /// <param name="playerLost"></param>
    public IEnumerator Complete(GameObject playerWon, GameObject playerLost) {
        var playerLostState = playerLost.GetComponent<PlayerStateManager>();
        var playerWonState = playerWon.GetComponent<PlayerStateManager>();

        // Complete the grapple state
        GrappleComplete = true;

        Debug.Log(playerWon.name + " dazes " + playerLost.name);

        // Enable movement for winning player
        playerWonState.TrySetState(PlayerStates.Default);
        // Daze loser for XX seconds
        playerLostState.TrySetState(PlayerStates.Dazed);

        yield return new WaitForSeconds(GrappleLoserDazeSeconds);

        // Let loser get up again
        playerLostState.TrySetState(PlayerStates.Default);

        ResetGrappleManager();
    }

    /// <summary>
    ///     Called once, by the player that initiated the grapple
    /// </summary>
    /// <param name="playerInitiated"></param>
    /// <param name="otherPlayer"></param>
    public void Initiate(GameObject playerInitiated, GameObject otherPlayer) {
        GrappleComplete = false;
        GrappleInitiated = true;
        Debug.Log(playerInitiated.name + " initiated grapple against " + otherPlayer.name);

        // Force both players into the grapple
        InitiateGrappleForPlayer(playerInitiated, otherPlayer);
        InitiateGrappleForPlayer(otherPlayer, playerInitiated);
    }

    /// <summary>
    ///     Can be called by either player. Resets the grapple to default state.
    /// </summary>
    private void ResetGrappleManager() {
        GrappleComplete = false;
        GrappleInitiated = false;
    }

    private void InitiateGrappleForPlayer(GameObject player, GameObject otherPlayer) {
        var config = player.GetComponent<PlayerConfig>();
        // Force grappling by both players (ignore checks)
        config.PlayerGrapple.EnableGrapple(otherPlayer);
        config.PlayerStateManager.ForceSetState(PlayerStates.Grappling);
    }
}