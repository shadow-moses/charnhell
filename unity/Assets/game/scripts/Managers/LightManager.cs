﻿using UnityEngine;

using DG.Tweening;

public class LightManager : MonoBehaviour {

    #region Singleton

    private static LightManager _instance;

    public static LightManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<LightManager>() ?? new GameObject("LightManager").AddComponent<LightManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
            {
                Destroy(gameObject);
            }
        }

        Init();
    }

    #endregion

    private SpriteRenderer spriteRenderer;
    private SpriteRenderer beamSpriteRenderer;

    public Collider2D player1Collider;
    public Collider2D player2Collider;

    public PlayerConfig player1Config;
    public PlayerConfig player2Config;

    // Gameplay values
    private bool active = false;
    public float balance = 0f;
    public float shiftRatePerSecond = 0.05f;

    // Light stay/hide times
    public float minLightStayTime = 3f;
    public float maxLightStayTime = 10f;
    public float minLightHideTime = 5f;
    public float maxLightHideTime = 10f;
    public float autoHideTime = 0f;
    public float autoShowTime = 0f;

    public Color neutralColor = new Color(1f, 1f, 1f);
    private float _intensity = 0f;
   
    void Init ()
    {
        beamSpriteRenderer = transform.FindChild("Beam").GetComponent<SpriteRenderer>();

        // Get our player objects
        GameObject player1 = GameManager.Instance.GetPlayer(PlayerNumbers.P1);
        GameObject player2 = GameManager.Instance.GetPlayer(PlayerNumbers.P2);

        spriteRenderer = GetComponent<SpriteRenderer>();

        // Get the colliders for all applicable players
        if (player1)
        {
            player1Collider = player1.GetComponent<Collider2D>();
            player1Config = player1.GetComponent<PlayerConfig>();
        }
        if (player2)
        {
            player2Collider = player2.GetComponent<Collider2D>();
            player2Config = player2.GetComponent<PlayerConfig>();
        }

        // Some initial settings so the light is hidden
        active = false;
        intensity = 0f;
        autoShowTime = Time.time + Random.Range(minLightHideTime, maxLightHideTime);
    }
	
	// Update is called once per frame
	void Update ()
    {
        ShowHideTimer();
        Pulse();
	}

    // When enabled
    public void OnEnable()
    {
        active = false;
        intensity = 0f;
        autoShowTime = Time.time + Random.Range(minLightHideTime, maxLightHideTime);
    }

    // When disabled
    public void OnDisable()
    {
        active = false;
        intensity = 0f;
        autoShowTime = Time.time + Random.Range(minLightHideTime, maxLightHideTime);
    }

    // Check the various timers to determine whether the light should be shown or hidden
    private void ShowHideTimer()
    {
        if (active && autoHideTime > 0f && autoHideTime < Time.time)
        {
            autoHideTime = 0f;
            HideLight();
        }
        else if (!active && autoShowTime > 0f && autoShowTime < Time.time)
        {
            autoShowTime = 0f;
            ShowLight();
        }
    }

    // Fade the light in at a random position and activate
    public void ShowLight() {
        transform.position = SpawnHelper.GetArenaSpawnLocation(GameManager.Instance.SpawnExtents);
        DOTween.To(() => intensity, x => intensity = x, Mathf.Abs(balance), 1.5f).OnComplete(() => {
            active = true;
            autoHideTime = Time.time + Random.Range(minLightHideTime, maxLightHideTime);
        });
    }

    // Fade the light out and deactivate
    public void HideLight()
    {
        DOTween.To(()=> intensity, x => intensity = x, 0f, 1.5f).OnComplete(() => {
            active = false;
            autoShowTime = Time.time + Random.Range(minLightStayTime, maxLightStayTime);
        });
    }

    // Check if players are touching the light, and if so alter the balance appropriately
    public void OnTriggerStay2D(Collider2D other)
    {
        if (!other.CompareTag(Tags.Player) || !active) {
            return;
        }
        if (other == player1Collider)
        {
            balance -= shiftRatePerSecond * Time.deltaTime;
        }
        else if (other == player2Collider)
        {
            balance += shiftRatePerSecond * Time.deltaTime;
        }
    }

    // Pulse the light up and down
    private void Pulse()
    {
        if (active)
        {
            float lightScale = Mathf.PerlinNoise(Time.time, 0.0f) * 0.8f;
            intensity = Mathf.Lerp(intensity, Mathf.Abs(balance) + lightScale, Time.deltaTime * 0.4f);
        }
    }

    // Get or set the intensity of the light, taking into account the current colour
    public float intensity {
        set
        {
            _intensity = value;
            transform.localScale = new Vector2(
                1f + (_intensity * 0.2f),
                1f + (_intensity * 0.2f)
            );

            _intensity = Mathf.Clamp(_intensity, 0f, 1f);

            Color lightColor;

            if (balance < 0)
            {
                lightColor = Color.Lerp(neutralColor, player1Config.PlayerColour, Mathf.Abs(balance));
            }
            else
            {
                lightColor = Color.Lerp(neutralColor, player2Config.PlayerColour, Mathf.Abs(balance));
            }

            lightColor.a = _intensity;

            spriteRenderer.color = lightColor;
            beamSpriteRenderer.color = lightColor;
        }
        get
        {
            return _intensity;
        }
    }
}
