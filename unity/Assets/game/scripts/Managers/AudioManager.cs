﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour {
    public AudioListener Listener;

    public AudioMixerGroup MusicGroup;
    public AudioMixerGroup AnnouncerGroup;
    public AudioMixerGroup AmbientGroup;
    public AudioMixerGroup PlayersGroup;
    public AudioMixerGroup EnemiesGroup;

    public int NoPlayerSounds = 8;
    public int NoEnemySounds = 8;
    public int NoAmbientSounds = 8;

    private AudioSource musicSource;
    private AudioSource announcerSource;
    private AudioSource player1Source;
    private AudioSource player2Source;

    private AudioSource[] ambientPool;
    private AudioSource[] enemySourcePool;

    private int lastEnemyIndex = 0;
    private int lastAmbientIndex = 0;

    #region Singleton

    private static AudioManager _instance;

    public static AudioManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<AudioManager>() ?? new GameObject("AudioManager").AddComponent<AudioManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    private void Awake() {
        if (_instance == null) {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        } else {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance) {
                Destroy(gameObject);
            }
        }

        Init();
    }

    #endregion

    private void Init() {
        // Set up audio source pools
        musicSource = AddAudioSource(MusicGroup);
        musicSource.loop = true;

        announcerSource = AddAudioSource(AnnouncerGroup);
        player1Source = AddAudioSource(PlayersGroup);
        player2Source = AddAudioSource(PlayersGroup);
        SetupPool(out ambientPool, AmbientGroup, NoAmbientSounds);
        SetupPool(out enemySourcePool, EnemiesGroup, NoEnemySounds);
    }

    /// <summary>
    ///     Plays a random audio clip
    /// </summary>
    /// <param name="audioPool"></param>
    /// <param name="clips"></param>
    public void PlayAudioClip(AudioPool audioPool, AudioClip[] clips) {
        if (clips == null || clips.Length <= 0) {
            return;
        }
        var index = Random.Range(0, clips.Length - 1);
        var clip = clips[index];

        PlayAudioClip(audioPool, clip);
    }

    public void PlayAudioClip(AudioPool audioPool, AudioClip clip) {
        if (clip == null || clip.length <= 0) {
            Debug.LogWarning("No audio clip can be found or the clip is empty");
            return;
        }

        switch (audioPool) {
            case AudioPool.Player1:
                PlayClip(player1Source, clip);
                break;
            case AudioPool.Player2:
                PlayClip(player2Source, clip);
                break;
            case AudioPool.Enemy:
                PlayAtNextIndex(enemySourcePool, clip, ref lastEnemyIndex);
                break;
            case AudioPool.Ambient:
                PlayAtNextIndex(ambientPool, clip, ref lastAmbientIndex);
                break;
            case AudioPool.Music:
                PlayClip(musicSource, clip);
                break;
            case AudioPool.Announcer:
                PlayClip(announcerSource, clip);
                break;
        }
    }

    private static void PlayAtNextIndex(AudioSource[] pool, AudioClip clip, ref int lastIndex) {
        lastIndex = lastIndex + 1;
        if (lastIndex >= pool.Length) {
            // Reset the index - i.e. overwrite the first sound played (even if its playing)
            lastIndex = 0;
        }
        var source = pool[lastIndex];
        PlayClip(source, clip);
    }

    private static void PlayClip(AudioSource source, AudioClip clip) {
        source.pitch = 1f; //Random.Range(-0.8f, 1.2f);
        source.clip = clip;
        source.Play();
    }

    private void SetupPool(out AudioSource[] pool, AudioMixerGroup group, int count) {
        var l = new List<AudioSource>();
        for (var i = 1; i < count; i++) {
            l.Add(AddAudioSource(group));
        }
        pool = l.ToArray();
    }

    private AudioSource AddAudioSource(AudioMixerGroup group) {
        var source = gameObject.AddComponent<AudioSource>();
        source.spatialBlend = 0;
        source.playOnAwake = false;
        source.outputAudioMixerGroup = group;
        return source;
    }
}