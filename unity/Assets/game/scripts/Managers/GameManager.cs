﻿using System;

using UnityEngine;

using DG.Tweening;

public class GameManager : MonoBehaviour {
    #region Singleton

    private static GameManager _instance;

    public static GameManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<GameManager>() ?? new GameObject("GameManager").AddComponent<GameManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    private void Awake() {
        if (_instance == null) {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        } else {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance) {
                Destroy(gameObject);
            }
        }
    }

    #endregion

    private GameObject[] _allPlayers;
    private BoxCollider2D _spawnExtents;
    private Camera _mainCamera;

    /// <summary>
    ///     Returns all players in the scene
    /// </summary>
    public GameObject[] AllPlayers {
        get { return _allPlayers ?? (_allPlayers = GameObject.FindGameObjectsWithTag(Tags.Player)); }
    }

    /// <summary>
    /// Returns all weapon items in the scene (not cached, as this item list changes often
    /// </summary>
    public GameObject[] AllWeapons {
        get { return GameObject.FindGameObjectsWithTag(Tags.WeaponItem); }
    }

    public GameObject GetPlayer(PlayerNumbers num) {
        foreach (GameObject player in AllPlayers) {
            if (player.GetComponent<PlayerConfig>().PlayerNumber == num) {
                return player;
            }
        }

        return null;
    }

    /// <summary>
    /// Shakes the camera by the specified magnitude and duration
    /// </summary>
    public void ShakeCamera(float magnitude = 0.04f, float duration = 0.3f)
    {
        MainCamera.DOShakePosition(duration, magnitude, 30, 90f);
    }

    /// <summary>
    /// Retrieves the spawn extents of the current arena
    /// </summary>
    public BoxCollider2D SpawnExtents {
        get {
            if (_spawnExtents != null) {
                return _spawnExtents;
            }

            // Lazy load
            var go = GameObject.Find("SpawnExtents");
            if (go == null) {
                Debug.LogError("Unable to find SpawnExtents GameObject. Each arena should have a SpawnExtents prefab with a BoxCollider2D attached.");
            }

            _spawnExtents = go.GetComponent<BoxCollider2D>();
            _spawnExtents.isTrigger = true;
            return _spawnExtents;
        }
    }

    /// <summary>
    /// Retrieves the scene's MainCamera
    /// </summary>
    public Camera MainCamera
    {
        get
        {
            if (_mainCamera != null)
            {
                return _mainCamera;
            }

            // Lazy load
            var go = GameObject.Find("MainCamera");
            if (go == null)
            {
                Debug.LogError("Unable to find MainCamera GameObject. Each arena should have a MainCamera Camera object.");
            }

            _mainCamera = go.GetComponent<Camera>();
            return _mainCamera;
        }
    }
}