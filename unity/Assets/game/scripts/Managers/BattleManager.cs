﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using DG;
using DG.Tweening;

public class BattleManager : MonoBehaviour {
    #region Singleton

    private static BattleManager _instance;

    public static BattleManager Instance {
        get {
            if (_instance == null) {
                _instance = FindObjectOfType<BattleManager>() ?? new GameObject("BattleManager").AddComponent<BattleManager>();

                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }

    private void Awake() {
        if (_instance == null) {
            //If I am the first instance, make me the Singleton
            _instance = this;
            DontDestroyOnLoad(this);
        } else {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance) {
                Destroy(gameObject);
            }
        }

        Init();
    }
    #endregion


    public Vector3 P1Start;
    public Vector3 P2Start;

    public PlayerConfig Player1;
    public PlayerConfig Player2;

    public Canvas UICanvas;
    public CanvasGroup UICanvasGroup;
    public Button UIButton;
    public Image OverlayImage;

    public Sprite StartScreen;
    public Sprite P1WinScreen;
    public Sprite P2WinScreen;
    public Sprite FightScreen;
    public Sprite ClickToPlay;
    public Sprite ClickToPlayAgain;

    public AudioMixerGroup MusicMixer;
    public AudioClip MainMusic;

    public AudioClip[] FightClips;

    public AudioClip[] AnnouncerClips;

    public bool GamePlayIsPaused = true;

    private const float P1WinState = -1;
    private const float P2WinState = 1;

    private float screenFadeTime = 1f;
    private bool battleEnded;
    private bool battleStarted;

    private EnemySpawner EnemySpawner;

    private void Init() {
        // Save players
        Player1 = GameManager.Instance.GetPlayer(PlayerNumbers.P1).GetComponent<PlayerConfig>();
        Player2 = GameManager.Instance.GetPlayer(PlayerNumbers.P2).GetComponent<PlayerConfig>();

        // Save player start positions
        P1Start = GameManager.Instance.GetPlayer(PlayerNumbers.P1).transform.position;
        P2Start = GameManager.Instance.GetPlayer(PlayerNumbers.P2).transform.position;

        EnemySpawner = GameObject.Find("EnemySpawner").GetComponent<EnemySpawner>();

        // Show start screen
        UICanvasGroup.alpha = 1f;
        OverlayImage.sprite = StartScreen;
        OverlayImage.enabled = true;
        ShowButton(ClickToPlay);
    }

    public void Start() {
        // Start music
        AudioManager.Instance.PlayAudioClip(AudioPool.Music, MainMusic);

        PauseGameplay();
    }

    public void Update() {
        MonitorWinState();

        MonitorStartKey();

        MonitorExitKey();
    }

    private void MonitorExitKey() {
        if (Input.GetKey(KeyCode.Escape)) {
            Application.Quit();
        }
    }

    private void MonitorStartKey() {
        if (!GamePlayIsPaused) {
            return;
        }
        if (battleEnded) {
            // Start timer
            StartCoroutine(DisableInputFor());
            return;
        }

        if (Input.anyKey) {
            StartBattle();
        }
    }

    private IEnumerator DisableInputFor() {
        yield return new WaitForSeconds(3f);

        battleEnded = false;
        ShowButton(ClickToPlayAgain);
    }

    private void PauseGameplay() {
        Player1.PlayerMovement.MovementEnabled = false;
        Player2.PlayerMovement.MovementEnabled = false;

        EnemySpawner.spawning = false;
        EnemySpawner.ToggleEnemies(false);

        LightManager.Instance.enabled = false;

        GamePlayIsPaused = true;
    }
    private void ResumeGameplay() {
        Player1.PlayerMovement.MovementEnabled = true;
        Player2.PlayerMovement.MovementEnabled = true;

        // Enemies
        EnemySpawner.spawning = true;
        EnemySpawner.ToggleEnemies(true);

        LightManager.Instance.enabled = true;

        GamePlayIsPaused = false;
    }

    private void MonitorWinState() {
        if (GamePlayIsPaused) {
            return;
        }

        if (LightManager.Instance.balance <= P1WinState) {
            EndBattle(PlayerNumbers.P1);
        } else if (LightManager.Instance.balance >= P2WinState) {
            EndBattle(PlayerNumbers.P1);
        }
    }

    // TEMP FOR TESTING
    public void EndBattleP1() {
        EndBattle(PlayerNumbers.P1);
    }

    private void EndBattle(PlayerNumbers player) {
        if (battleEnded) {
            return;
        }

        battleEnded = true;
        battleStarted = false;

        PauseGameplay();

        switch (player) {
            case PlayerNumbers.P1:
                FadeInOverlayImage(P1WinScreen, screenFadeTime);
                break;
            case PlayerNumbers.P2:
                FadeInOverlayImage(P2WinScreen, screenFadeTime);
                break;
        }

        // Destroy all weapons
        foreach (var w in GameManager.Instance.AllWeapons) {
            Destroy(w);
        }

        // destroy all enemies
        EnemySpawner.DestroyEnemies();

        HideButton();
    }

    public void StartBattle() {
        if (battleStarted) {
            return;
        }

        battleStarted = true;

        // reset players
        Player1.transform.position = P1Start;
        Player2.transform.position = P2Start;

        // reset balance / light
        LightManager.Instance.balance = 0;

        // Fight screen
        StartCoroutine(ShowFightScreen());
    }

    private void ShowButton(Sprite sprite) {
        UIButton.gameObject.SetActive(true);
        UIButton.image.sprite = sprite;
    }

    private void HideButton() {
        UIButton.gameObject.SetActive(false);
    }

    private IEnumerator ShowFightScreen() {
        HideButton();

        FadeOutOverlayImage(.3f);
        yield return new WaitForSeconds(.3f);

        FadeInOverlayImage(FightScreen, .1f);
        AudioManager.Instance.PlayAudioClip(AudioPool.Announcer, FightClips);
        GameManager.Instance.ShakeCamera();

        yield return new WaitForSeconds(1.5f);

        FadeOutOverlayImage(.1f);

        ResumeGameplay();
    }

    private void FadeInOverlayImage(Sprite sprite, float fadeSpeed) {
        UICanvasGroup.alpha = 0;
        OverlayImage.sprite = sprite;
        OverlayImage.enabled = true;

        UICanvasGroup.DOFade(1f, fadeSpeed);
    }

    private void FadeOutOverlayImage(float fadeSpeed) {
        UICanvasGroup.DOFade(0f, fadeSpeed)
            .OnComplete(() => {
                OverlayImage.enabled = false;
            });
    }

}
