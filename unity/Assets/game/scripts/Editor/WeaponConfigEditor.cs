﻿using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

[CustomEditor(typeof (WeaponConfig))]
public class WeaponConfigEditor : Editor {
    private WeaponConfig _target;

    private Rigidbody2D _rigidbody;

    public void Awake() {
        _target = target as WeaponConfig;
        _rigidbody = _target.GetComponent<Rigidbody2D>();
    }

    #region Overrides of Editor

    public override void OnInspectorGUI() {
        // Create the default states
        SetupDefaultStates();

        // Draw the inspector as usual
        DrawDefaultInspector();
    }

    #endregion

    private void SetupDefaultStates() {
        if (_target.AllStates != null && _target.AllStates.Length > 0) {
            return;
        }

        // by default, all states are set up with the rigidbody's settings
        var defaultState = new WeaponConfig.WeaponState {
            // OnGround should use the default rigidbody settings for this component
            AngularDrag = _rigidbody.angularDrag,
            DetectionMode = _rigidbody.collisionDetectionMode,
            IsKinematic = _rigidbody.isKinematic,
            LinearDrag = _rigidbody.drag,
            Mass = _rigidbody.mass,
            ColliderEnabled = true
        };

        var beingHeld = defaultState.Clone();
        beingHeld.State = WeaponConfig.WeaponStates.BeingHeld;
        beingHeld.ColliderEnabled = true;
        beingHeld.DetectionMode = CollisionDetectionMode2D.None;
        beingHeld.IsKinematic = true;

        var onGroundStatic = defaultState.Clone();
        onGroundStatic.State = WeaponConfig.WeaponStates.OnGroundStatic;
        onGroundStatic.ColliderEnabled = false; // No collider when on ground
        onGroundStatic.DetectionMode = CollisionDetectionMode2D.None; // No detection on ground
        onGroundStatic.AngularDrag = 0;
        onGroundStatic.LinearDrag = 0;
        onGroundStatic.Mass = 0;
        onGroundStatic.IsKinematic = true;

        var onGroundMoving = defaultState.Clone();
        onGroundMoving.State = WeaponConfig.WeaponStates.OnGroundMoving;
        onGroundMoving.ColliderEnabled = true; // so we can detect other physics interactions
        onGroundMoving.DetectionMode = CollisionDetectionMode2D.Continuous;

        var inAir = defaultState.Clone();
        inAir.State = WeaponConfig.WeaponStates.InAir;
        inAir.ColliderEnabled = true;
        inAir.DetectionMode = CollisionDetectionMode2D.Continuous;

        var states = new List<WeaponConfig.WeaponState> {
            onGroundStatic,
            onGroundMoving,
            beingHeld,
            inAir
        };

        _target.AllStates = states.ToArray();
    }
}