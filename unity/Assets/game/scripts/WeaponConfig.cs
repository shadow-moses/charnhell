﻿using System;
using System.Linq;

using UnityEngine;

[RequireComponent(typeof (Rigidbody2D))]
public class WeaponConfig : MonoBehaviour {
    /// <summary>
    /// The state that the object starts in
    /// </summary>
    public WeaponStates StartState = WeaponStates.OnGroundStatic;

    /// <summary>
    /// The sound that will be played when the item is equipped
    /// </summary>
    public AudioClip PickupClip;

    /// <summary>
    /// The current state of the object
    /// </summary>
    public WeaponStates CurrentState { get; private set; }

    public WeaponState[] AllStates;

    public Rigidbody2D WeaponRigidbody { get; private set; }

    private Collider2D _weaponCollider;
    private bool _collided;

    public void Awake() {
        WeaponRigidbody = GetComponent<Rigidbody2D>();
        _weaponCollider = GetComponent<Collider2D>();

        // All weapons are on ground by default
        SetState(StartState);
    }

    /// <summary>
    ///     Sets the object to the state passed
    /// </summary>
    /// <param name="state"></param>
    public void SetState(WeaponStates state) {
        // Get the state out of the pool
        if (AllStates == null || AllStates.Length <= 0) {
            Debug.LogError(string.Format("No states have been set up for the weapon named {0} @ {1}",
                gameObject.name, transform.position));
        }

        var foundState = GetStateSettings(state);

        // TODO: Check if there are any missing states or duplicates

        // Set the state
        Debug.Log(string.Format("Changing state of {0} from {1} to {2}", name, CurrentState, state));
        CurrentState = state;

        // Set the relevant settings
        SetupRigidbodyFromState(foundState);
        SetupColliderFromState(foundState);
    }

    /// <summary>
    /// Retrieves the state settings for the current state
    /// </summary>
    /// <returns></returns>
    public WeaponState GetStateSettings() {
        return GetStateSettings(CurrentState);
    }

    /// <summary>
    /// Retrieves the state settings for the specified state
    /// </summary>
    /// <param name="state"></param>
    /// <returns></returns>
    private WeaponState GetStateSettings(WeaponStates state) {
        var foundState = AllStates.FirstOrDefault(x => x.State == state);
        if (foundState == null) {
            Debug.LogError(string.Format("{0} state has not been set up for the weapon named {1} @ {2}",
                state, gameObject.name, transform.position));
        }
        return foundState;
    }

    private void SetupRigidbodyFromState(WeaponState state) {
        WeaponRigidbody.drag = state.LinearDrag;
        WeaponRigidbody.angularDrag = state.AngularDrag;
        WeaponRigidbody.mass = state.Mass;
        WeaponRigidbody.isKinematic = state.IsKinematic;
        WeaponRigidbody.collisionDetectionMode = state.DetectionMode;
    }

    private void SetupColliderFromState(WeaponState state) {
        _weaponCollider.enabled = state.ColliderEnabled;
    }

    public void OnCollisionEnter2D(Collision2D collision) {
        // Tell LateUpdate that a collision has occurred
        if (CurrentState == WeaponStates.BeingHeld) {
            // Do not update state if the weapon is being held
            return;
        }
        _collided = true;
    }

    public void FixedUpdate() {
        UpdateMovingState();
    }

    private void UpdateMovingState() {
        switch (CurrentState) {
            case WeaponStates.InAir:
                if (WeaponRigidbody.velocity.sqrMagnitude > 0 && WeaponRigidbody.velocity.sqrMagnitude <= 10) {
                    SetState(WeaponStates.OnGroundMoving);
                }
                break;
            case WeaponStates.OnGroundMoving:
                if (WeaponRigidbody.velocity.sqrMagnitude <= 0.1f) {
                    SetState(WeaponStates.OnGroundStatic);
                }
                break;
        }
    }

    public void LateUpdate() {
        UpdateMovementPhysics();
    }

    private void UpdateMovementPhysics() {
        if (!_collided) {
            return;
        }

        // When the weapon collides with something, change it to the on ground state
        SetState(WeaponStates.OnGroundMoving);
        _collided = false;
    }

    [Serializable]
    public class WeaponState {
        public WeaponStates State;

        // Rigidbody settings
        public float LinearDrag;
        public float AngularDrag;
        public float Mass;
        public bool IsKinematic;
        public CollisionDetectionMode2D DetectionMode;

        // Collider settings
        public bool ColliderEnabled = true;

        // Damage settings
        public float DamageMultiplier = 1f;

        #region Implementation of ICloneable

        public WeaponState Clone() {
            return (WeaponState)MemberwiseClone();
        }

        #endregion
    }

    public enum WeaponStates {
        OnGroundStatic,
        OnGroundMoving,
        BeingHeld,
        InAir
    }
}