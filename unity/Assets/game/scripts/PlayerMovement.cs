﻿using UnityEngine;

[RequireComponent(typeof (PlayerConfig))]
public class PlayerMovement : MonoBehaviour {
    public float Speed = 1.5f;

    public float MaxDashForce = 500f;
    public float MaxDashHoldTime = 2f;
    public float MinDashHoldTime = .5f;
    public float DashDelay = .7f;

    /// <summary>
    /// Used to determine the last button pressed by the player
    /// </summary>
    public Vector3 LastDirection { get; private set; }

    /// <summary>
    ///     If true, the player is currently charging a dash
    /// </summary>
    public bool DashCharging { get; private set; }

    /// <summary>
    /// If set to false, movement is disabled for this player
    /// </summary>
    public bool MovementEnabled { get; set; }

    private PlayerConfig _config;
    private ButtonPress[] presses;
    private float timeSinceLastDash;
    private float timeDashHeld;

    public void Awake() {
        _config = GetComponent<PlayerConfig>();

        // Register movement buttons
        presses = new[] {
            new ButtonPress {
                ButtonName = InputSettings.VertAxis,
                MecanimVariable = AnimationSettings.MecanimNorth,
                WorldDirection = Vector2.up,
                AxisDir = AxisDir.Positive
            },
            new ButtonPress {
                ButtonName = InputSettings.VertAxis,
                MecanimVariable = AnimationSettings.MecanimSouth,
                WorldDirection = -Vector2.up,
                AxisDir = AxisDir.Negative
            },
            new ButtonPress {
                ButtonName = InputSettings.HorizAxis,
                MecanimVariable = AnimationSettings.MecanimEast,
                WorldDirection = Vector2.right,
                AxisDir = AxisDir.Positive
            },
            new ButtonPress {
                ButtonName = InputSettings.HorizAxis,
                MecanimVariable = AnimationSettings.MecanimWest,
                WorldDirection = -Vector2.right,
                AxisDir = AxisDir.Negative
            }
        };

        // movement allowed by default
        MovementEnabled = true;
    }

    public void Start() {
    }

    public void Update() {
        UpdateMovement();

        timeSinceLastDash += Time.deltaTime;
    }

    #region Update methods

    private void UpdateMovement() {
        if (!MovementEnabled) {
            // Unable to move
            return;
        }

        var dir = Vector2.zero;

        foreach (var p in presses) {
            dir += UpdateMovement(p);
        }

        // Normalize direction
        dir = dir.normalized;

        if (dir != Vector2.zero) {
            // Move the rigidbody
            _config.PlayerRigidbody.AddForce(dir*Speed);

            // Save the last direction for other scripts to use
            LastDirection = dir;

            // Try to perform a dash in the specified direction
            UpdateDash(dir);

            _config.PlayerAnimator.SetBool(AnimationSettings.MecanimMoving, true);
        } else {
            // Not moving
            _config.PlayerAnimator.SetBool(AnimationSettings.MecanimMoving, false);
        }
    }

    #endregion

    #region Helpers

    private Vector2 UpdateMovement(ButtonPress press) {
        var m_dpad = InputHelper.GetPlayerControl(_config.PlayerNumber, press.ButtonName, "DPad");
        var m_stick = InputHelper.GetPlayerControl(_config.PlayerNumber, press.ButtonName, "Stick");
        var m_keyb = InputHelper.GetPlayerControl(_config.PlayerNumber, press.ButtonName, "Keyboard");

        var dpadValue = Input.GetAxisRaw(m_dpad);
        var stickValue = Input.GetAxisRaw(m_stick);
        var keybValue = Input.GetAxisRaw(m_keyb);

        var directionHeld = InputHelper.OutOfDeadZone(dpadValue, press.AxisDir) || InputHelper.OutOfDeadZone(stickValue, press.AxisDir) || InputHelper.OutOfDeadZone(keybValue, press.AxisDir);

        // Animate
        _config.PlayerAnimator.SetBool(press.MecanimVariable, directionHeld);

        if (directionHeld) {
            //Debug.Log(press.ButtonName + " " + m_dpad + ", " + m_stick + " " + m_keyb);
            return press.WorldDirection;
        }
        

        return Vector3.zero;
    }

    /// <summary>
    ///     Is only called when a movement button is held down
    /// </summary>
    /// <param name="dashDirection"></param>
    private void UpdateDash(Vector2 dashDirection) {
        var dash = InputHelper.GetPlayerControl(_config.PlayerNumber, InputSettings.Dash);
        var dashUp = Input.GetButtonUp(dash);
        var dashHeld = Input.GetButton(dash);

        if (dashHeld) {
            // Dash held, increment timer
            timeDashHeld += Time.deltaTime;
            DashCharging = true;
        } else if (dashUp) {
            // dash released, dash based on delta
            PerformDash(dashDirection);
        }
    }

    /// <summary>
    ///     Resets all dash timers
    /// </summary>
    private void ResetDash() {
        timeSinceLastDash = 0f;
        timeDashHeld = 0f;
        DashCharging = false;
    }

    private void PerformDash(Vector2 dashDirection) {
        if (timeSinceLastDash < DashDelay) {
            // Trying to dash too quickly
            return;
        }

        //Debug.Log(config.PlayerNumber + " dashing");

        // Apply the dash force in the specified direction
        var force = MaxDashForce*Mathf.Clamp(timeDashHeld, MinDashHoldTime, MaxDashHoldTime);
        _config.PlayerRigidbody.AddForce(dashDirection.normalized * force);

        ResetDash();
    }

    private class ButtonPress {
        public string ButtonName { get; set; }
        public string MecanimVariable { get; set; }
        public Vector2 WorldDirection { get; set; }
        public AxisDir AxisDir { get; set; }
    }

    #endregion
}