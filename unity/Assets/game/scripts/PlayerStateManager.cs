﻿using UnityEngine;

public class PlayerStateManager : MonoBehaviour {
    public PlayerStates CurrentState { get; private set; }

    private PlayerConfig _config;

    public void Awake() {
        _config = GetComponent<PlayerConfig>();
    }

    /// <summary>
    ///     Validates and sets the specified state
    /// </summary>
    /// <param name="state"></param>
    public bool TrySetState(PlayerStates state) {
        var canSetState = TryState(state);
        if (!canSetState) {
            return false;
        }

        ForceSetState(state);

        return true;
    }

    /// <summary>
    /// Forces the specified state
    /// </summary>
    /// <param name="state"></param>
    /// <returns></returns>
    public void ForceSetState(PlayerStates state) {
        // State was successful, set the state!
        switch (state) {
            case PlayerStates.Default:
                SetDefault();
                break;
            case PlayerStates.Dazed:
                SetDazed();
                break;
            case PlayerStates.Grappling:
                SetGrappling();
                break;
            case PlayerStates.HoldingWeapon:
                SetHoldingWeapon();
                break;
            case PlayerStates.Dead:
                SetDeath();
                break;
        }

        CurrentState = state;
    }

    /// <summary>
    /// Checks if the state can be set, but does not set it
    /// </summary>
    /// <param name="state"></param>
    /// <returns></returns>
    public bool TryState(PlayerStates state) {
        var stateSet = false;
        switch (state) {
            case PlayerStates.Default:
                stateSet = TryDefault();
                break;
            case PlayerStates.Dazed:
                stateSet = TryDazed();
                break;
            case PlayerStates.Grappling:
                stateSet = TryGrapple();
                break;
            case PlayerStates.HoldingWeapon:
                stateSet = TryHoldingWeapon();
                break;
            case PlayerStates.Dead:
                stateSet = TryDeath();
                break;
        }
        return stateSet;
    }

    #region States

    private void SetDefault() {
        _config.PlayerMovement.MovementEnabled = true;
        _config.PlayerRigidbody.isKinematic = false;

        _config.PlayerAnimator.SetBool(AnimationSettings.MecanimGrapple, false);
        _config.PlayerAnimator.SetBool(AnimationSettings.MecanimDazed, false);

        // Drop any held weapons
        _config.PlayerWeaponHandler.ReleaseWeapon();
        // Cancel any grapple settings
        _config.PlayerGrapple.CancelGrapple();
    }

    private void SetDazed() {
        // Disable player and play daze animation
        _config.PlayerMovement.MovementEnabled = false;
        _config.PlayerRigidbody.isKinematic = true;
        _config.PlayerAnimator.SetBool(AnimationSettings.MecanimGrapple, false);
        _config.PlayerAnimator.SetBool(AnimationSettings.MecanimDazed, true);

        // Drop any held weapons
        _config.PlayerWeaponHandler.ReleaseWeapon();
    }

    private void SetGrappling() {
        _config.PlayerMovement.MovementEnabled = false;
        _config.PlayerRigidbody.isKinematic = true;
        // TODO Face the other player

        // Set layer param to animate grapple in direction
        _config.PlayerAnimator.SetBool(AnimationSettings.MecanimDazed, false);
        _config.PlayerAnimator.SetBool(AnimationSettings.MecanimGrapple, true);

        // Drop any held weapons
        _config.PlayerWeaponHandler.ReleaseWeapon();
    }

    private void SetHoldingWeapon() {
        _config.PlayerMovement.MovementEnabled = true;
        _config.PlayerRigidbody.isKinematic = false;

        _config.PlayerAnimator.SetBool(AnimationSettings.MecanimGrapple, false);
        _config.PlayerAnimator.SetBool(AnimationSettings.MecanimDazed, false);
    }

    private void SetDeath() {
        _config.PlayerMovement.MovementEnabled = false;
        _config.PlayerRigidbody.isKinematic = true;

        _config.PlayerAnimator.SetBool(AnimationSettings.MecanimGrapple, false);
        _config.PlayerAnimator.SetBool(AnimationSettings.MecanimDazed, false);

        // Drop any held weapons
        _config.PlayerWeaponHandler.ReleaseWeapon();
    }

    #endregion

    #region Tries

    private bool TryDefault() {
        return true;
    }

    private bool TryDazed() {
        switch (CurrentState) {
            case PlayerStates.Dazed:
            case PlayerStates.Dead:
                // Cannot Daze if dead or already dazed
                return false;
        }
        return true;
    }

    private bool TryDeath() {
        switch (CurrentState) {
            case PlayerStates.Dead:
                // Cannot die twice!
                Debug.Log("Already dead");
                return false;
        }
        return true;
    }

    private bool TryGrapple() {
        switch (CurrentState) {
            case PlayerStates.Dazed:
            case PlayerStates.Dead:
            case PlayerStates.HoldingWeapon:
                // player cannot initiate a grapple if they are dead, dazed or holding a weapon
                return false;
        }
        return true;
    }

    private bool TryHoldingWeapon() {
        if (CurrentState != PlayerStates.Default) {
            // player cannot pick up a weapon unless they are in the default state
            return false;
        }
        return true;
    }

    #endregion
}