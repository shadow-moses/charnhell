﻿using System;
using System.Collections;

using DG.Tweening;

using UnityEngine;

[RequireComponent(typeof (Animator))]
[RequireComponent(typeof (Collider2D))]
public class DamageHandler : MonoBehaviour {
    /// <summary>
    ///     If true, this is a player that will respawn instead of die
    /// </summary>
    public bool IsPlayer;

    public float TotalHealth = 100f;
    public float PlayerCorpseMinDuration = 5f;
    public float PlayerCorpseMaxDuration = 10f;
    public float PlayerMinRespawnDuration = 1f;
    public float PlayerMaxRespawnDuration = 5f;

    /// <summary>
    ///     If IsPlayer is true, one of these sprites will be used in place of the dead player
    /// </summary>
    public Sprite[] PlayerDeadSprites;

    /// <summary>
    ///     The objects randomly spawned by object's death
    /// </summary>
    public GameObject[] SpawnedOnDeath;

    /// <summary>
    /// The probability each object in the list will be spawned
    /// </summary>
    [Range(0, 1)]
    public float SpawnedOnDeathProbability = 1f;

    /// <summary>
    ///     A bank of sounds that can be played when the object takes damage
    /// </summary>
    public AudioClip[] HurtClips;

    /// <summary>
    ///     A bank of sounds that can be played when the object dies
    /// </summary>
    public AudioClip[] DeathClips;

    /// <summary>
    /// The minimum damage force this object can receive before taking damage
    /// </summary>
    public float MinDamageForce = 5f;

    /// <summary>
    /// The amount of time (in seconds) before the object can take damage again
    /// </summary>
    public float DamageInvulnerabilityTime = .5f;

    /// <summary>
    /// If false, no damage flash will be performed.
    /// </summary>
    public bool DamageFlash = true;

    /// <summary>
    /// The particle effect to play when the item spawns
    /// </summary>
    public GameObject SpawnEffectParticlePrefab;

    private float _currentHealth = 100f;
    private PlayerConfig _config;
    private Tweener damageTween;

    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private Color defaultSpriteColour;
    private AudioPool audioPool;

    private void Awake() {
        _currentHealth = TotalHealth;

        if (IsPlayer) {
            _config = GetComponent<PlayerConfig>();
            animator = _config.PlayerAnimator;
            spriteRenderer = _config.PlayerSpriteRenderer;
            defaultSpriteColour = _config.PlayerColour;
            audioPool = _config.PlayerAudioPool;
        } else {
            animator = GetComponent<Animator>();
            spriteRenderer = GetComponent<SpriteRenderer>();
            defaultSpriteColour = spriteRenderer.color;
            audioPool = AudioPool.Enemy;
        }

        if (IsPlayer && (PlayerDeadSprites == null || PlayerDeadSprites.Length <= 0)) {
            Debug.LogError("PlayerDeadSprites should be populated if IsPlayer is true");
        }
    }

    private float _lastDamageTime;

    public void Damage(float damage) {

        if ((Time.time - _lastDamageTime) <= DamageInvulnerabilityTime) {
            // Object is in invincibility frames
            return;
        }

        // Proceed to damage
        _lastDamageTime = Time.time;
        _currentHealth = _currentHealth - damage;

        if (_currentHealth <= 0f) {
            if (IsPlayer) {
                // Player taking damage, do state check
                var canDie = _config.PlayerStateManager.TrySetState(PlayerStates.Dead);
                if (!canDie) {
                    return;
                }
            }

            // no more health, die
            StartCoroutine(Die());
            return;
        }

        // Still alive

        // Flash
        StartDamageTween();

        // Play a hurt sound to the relevant pool
        AudioManager.Instance.PlayAudioClip(audioPool, HurtClips);
    }

    public void OnCollisionEnter2D(Collision2D collision) {
        // Only weapons and moveable objects can cause damage
        if (!collision.gameObject.CompareTag(Tags.WeaponItem)) {
            // This item cannot damage by impact
            return;
        }

        Debug.Log(transform.name + " detected collision with " + collision.gameObject.name);

        var weaponConfig = collision.gameObject.GetComponent<WeaponConfig>();
        if (weaponConfig == null) {
            Debug.LogError(_config.PlayerNumber + " weapon config is not applied to player");
            return;
        }

        if (IsPlayer) {
            // If this weapon is being held by the current player, do not damage / collider
            if (weaponConfig.CurrentState == WeaponConfig.WeaponStates.BeingHeld) {
                if (_config.PlayerWeaponHandler.HeldWeapon == null || weaponConfig == _config.PlayerWeaponHandler.HeldWeapon) {
                    return;
                }
            }
        }

        var damageForce = collision.rigidbody.mass * collision.relativeVelocity.magnitude;
        damageForce = damageForce * weaponConfig.GetStateSettings().DamageMultiplier;

        if (damageForce > MinDamageForce) {
            Debug.Log(transform.name + " takes " + damageForce + " damage");

            Damage(damageForce);
        }
    }

    /// <summary>
    ///     Destroys the object and performs all death functions
    /// </summary>
    /// <returns></returns>
    private IEnumerator Die() {
        GameManager.Instance.ShakeCamera(0.06f, 0.5f);

        // Disable animator
        animator.enabled = false;

        // Something has died, play a random announcement
        AudioManager.Instance.PlayAudioClip(AudioPool.Announcer, BattleManager.Instance.AnnouncerClips);

        // Play a random death sound in the relevant audio pool
        AudioManager.Instance.PlayAudioClip(audioPool, DeathClips);

        if (IsPlayer) {
            // This is a player

            // Temporarily disable the player's main components
            _config.PlayerSpriteRenderer.enabled = false;
            _config.PlayerMovement.MovementEnabled = false;
            _config.MovementCollider.enabled = false;

            // Create a new object in place of the player (with no collider etc)
            var corpse = CreateCorpse();

            // Wait for a random time
            var respawnDuration = UnityEngine.Random.Range(PlayerMinRespawnDuration, PlayerMaxRespawnDuration);

            yield return new WaitForSeconds(respawnDuration);

            // Reset / respawn the player
            RespawnPlayer();

            // Wait for XX seconds before removing the corpse / spawining any objects
            var corpseTime = UnityEngine.Random.Range(PlayerCorpseMinDuration, PlayerCorpseMaxDuration) - respawnDuration;
            yield return new WaitForSeconds(corpseTime);

            // Spawn random objects
            SpawnObjects(corpse.transform.position);

            // Destroy the corpse
            Destroy(corpse);
        } else {
            // This is an enemy or object, spawn other objects, then destroy the object (no corpse)
            SpawnObjects(transform.position);

            Destroy(gameObject);
        }
        GameManager.Instance.ShakeCamera(0.06f, 0.5f);
    }

    /// <summary>
    ///     Resets all settings for the player, so they can respawn with full health / animation etc
    /// </summary>
    private void ResetPlayerSettings() {
        _config.PlayerSpriteRenderer.enabled = true;
        animator.enabled = true;
        _currentHealth = TotalHealth;
        _config.PlayerMovement.MovementEnabled = true;
        _config.MovementCollider.enabled = true;
        // Move player back into the default state
        var canDefault = _config.PlayerStateManager.TrySetState(PlayerStates.Default);
        if (!canDefault) {
            Debug.LogError(_config.PlayerNumber + " unable to move from death to default state. Remains dead.");
        }

        ResetDamageTween();
    }

    /// <summary>
    /// Initiates the damage tween
    /// </summary>
    private void StartDamageTween() {
        if (!DamageFlash) {
            return;
        }

        ResetDamageTween();

        damageTween = spriteRenderer
            .DOColor(new Color(defaultSpriteColour.r, defaultSpriteColour.g, defaultSpriteColour.b, .8f), .05f)
            .SetLoops(12, LoopType.Yoyo);

        GameManager.Instance.ShakeCamera();
    }

    /// <summary>
    /// Resets any changes made by the damage tween
    /// </summary>
    private void ResetDamageTween() {
        if (damageTween != null && damageTween.IsPlaying()) {
            damageTween.Kill();
        }

        // reset colour back to start
        _config.PlayerSpriteRenderer.color = defaultSpriteColour;
    }

    /// <summary>
    ///     Resets the object to normal and randomly places it in the level
    /// </summary>
    private void RespawnPlayer() {
        ResetPlayerSettings();

        // Randomly place player in arena
        transform.position = SpawnHelper.GetArenaSpawnLocation(GameManager.Instance.SpawnExtents);
    }

    /// <summary>
    ///     Spawns the objects specified in SpawnedOnDeath
    /// </summary>
    private void SpawnObjects(Vector3 atPosition) {
        if (SpawnedOnDeath == null || SpawnedOnDeath.Length <= 0) {
            return;
        }

        // Spawn the effect prefab
        SpawnParticleEffect(atPosition);

        foreach (var o in SpawnedOnDeath) {
            // Random chance of spawn
            var shouldSpawn = UnityEngine.Random.value <= SpawnedOnDeathProbability;
            if (!shouldSpawn) {
                continue;
            }
            var newWeapon = Instantiate(o);
            newWeapon.transform.position = atPosition;
            newWeapon.transform.Rotate(Vector3.forward, UnityEngine.Random.Range(1f, 360f));
            var rb = newWeapon.GetComponent<Rigidbody2D>();
            var wConfig = newWeapon.GetComponent<WeaponConfig>();
            if (rb != null) {
                // Add force
                wConfig.SetState(WeaponConfig.WeaponStates.InAir);

                rb.AddRelativeForce(Vector2.right*UnityEngine.Random.Range(50f, 65f));
                rb.AddTorque(UnityEngine.Random.Range(-0.1f, 0.1f));
            }
        }
    }

    /// <summary>
    /// Spawns the death animation particle effect, if set
    /// </summary>
    /// <param name="atPosition"></param>
    private void SpawnParticleEffect(Vector3 atPosition) {
        if (SpawnEffectParticlePrefab == null) {
            return;
        }

        var particleEffect = Instantiate(SpawnEffectParticlePrefab);
        if (particleEffect == null) {
            Debug.LogError(_config.PlayerNumber + " no SpawnEffectParticlePrefab set. Unable to spawn particles.");
            return;
        }

        particleEffect.transform.position = atPosition;
        var ps = particleEffect.GetComponent<ParticleSystem>();
        ps.Play();
        Destroy(particleEffect, ps.duration * 2f);
    }

    /// <summary>
    ///     Creates a corpse using a random death sprite
    /// </summary>
    /// <returns></returns>
    private GameObject CreateCorpse() {
        var corpse = new GameObject(string.Concat(gameObject.name, "_corpse_", Time.time));
        corpse.transform.position = transform.position;
        corpse.transform.rotation = transform.rotation;
        corpse.transform.localScale = transform.localScale;

        var r = corpse.AddComponent<SpriteRenderer>();
        r.sprite = PlayerDeadSprites[UnityEngine.Random.Range(0, PlayerDeadSprites.Length - 1)];
        r.sortingLayerName = SortingLayers.BackgroundObjects; // Players can walk over corpses, but they are on top of the background
        r.color = defaultSpriteColour;
        return corpse;
    }
}