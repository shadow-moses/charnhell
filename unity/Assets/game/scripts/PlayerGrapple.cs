﻿using UnityEngine;

/// <summary>
///     Handles grappling
/// </summary>
[RequireComponent(typeof (PlayerConfig))]
public class PlayerGrapple : MonoBehaviour {
    public AudioClip GrappleClip;
    public AudioClip GrappleWinClip;

    private PlayerConfig _config;
    private Animator _animator;
    private PlayerStateManager _state;

    private int GrapplePressCount = 0;
    private bool CanGrapple = false;
    private GameObject otherPlayer;

    public void Awake() {
        _config = GetComponent<PlayerConfig>();
        _animator = GetComponentInChildren<Animator>();
        _state = GetComponentInChildren<PlayerStateManager>();
    }

    private void Update() {
        UpdateGrapple();
    }

    private void UpdateGrapple() {
        if (!CanGrapple) {
            // Grappling is disabled for some reason
            return;
        }

        if (otherPlayer == null) {
            Debug.LogError(_config.PlayerNumber + " otherPlayerCollider is not set");
            return;
        }

        if (GrappleManager.Instance.GrappleComplete) {
            // Grapple has been won by a player, stop checking for grapple updates
            return;
        }

        var buttonName = InputHelper.GetPlayerControl(_config.PlayerNumber, InputSettings.ActionButton);
        var attackDown = Input.GetButtonDown(buttonName);
        var attackUp = Input.GetButtonUp(buttonName);

        if (attackDown) {
            GameManager.Instance.ShakeCamera(0.03f, 0.4f);
            InitiateGrapple();

            // Set the state for this player
            _state.TrySetState(PlayerStates.Grappling);

            // Increment the grapple counter for each button press
            GrapplePressCount = GrapplePressCount + 1;
            Debug.Log(_config.PlayerNumber + " grapple count @ " + GrapplePressCount);

            // Play grapple sound using pool
            AudioManager.Instance.PlayAudioClip(_config.PlayerAudioPool, GrappleClip);

            // Fire off a quick grapple attack in the right direction
            _animator.SetBool(AnimationSettings.MecanimGrappleQuick, true);

            if (GrapplePressCount >= GrappleManager.GrapplePressesNeeded) {
                // Player has reached the grapple count
                Debug.Log(_config.PlayerNumber + " grapple WON");

                AudioManager.Instance.PlayAudioClip(_config.PlayerAudioPool, GrappleWinClip);

                StartCoroutine(GrappleManager.Instance.Complete(gameObject, otherPlayer));

                CancelGrapple();

                _state.TrySetState(PlayerStates.Default);
            }
        } else if (attackUp) {
            // Stop any quick animation playing
            //animator.SetBool(AnimationSettings.MecanimGrapple, false);
            _animator.SetBool(AnimationSettings.MecanimGrappleQuick, false);
        }
    }

    /// <summary>
    ///     Sets up a player ready to grapple another player (grapple is NOT initiated here)
    /// </summary>
    /// <param name="other"></param>
    public void EnableGrapple(GameObject other) {
        Debug.Log(_config.PlayerNumber + " Grapple enabled");
        GrapplePressCount = 0;
        CanGrapple = true;
        otherPlayer = other;
    }

    /// <summary>
    /// Cancels grapple initiation
    /// </summary>
    public void CancelGrapple() {
        Debug.Log(_config.PlayerNumber + " Grapple canceled");
        GrapplePressCount = 0;
        CanGrapple = false;
        otherPlayer = null;
    }

    /// <summary>
    ///     Initiates the grapple in the manager sets the Grappling state
    /// </summary>
    private void InitiateGrapple() {
        if (GrappleManager.Instance.GrappleInitiated) {
            // Grapple has already been initiated by another player, no need to reinitialize
            return;
        }

        Debug.Log(_config.PlayerNumber + " grapple initiated");
        GrappleManager.Instance.Initiate(gameObject, otherPlayer.gameObject);
    }

    public void OnTriggerEnter2D(Collider2D other) {
        if (other.isTrigger) {
            // Ignore trigger colliders - we only want physics colliders
            return;
        }

        // Get the other player
        if (!other.CompareTag(Tags.Player)) {
            //Debug.Log(config.PlayerNumber + " collider not other player");
            return;
        }

        if (GrappleManager.Instance.GrappleComplete) {
            // Grapple has been won by a player, stop checking for grapple updates
            return;
        }

        if (!_state.TryState(PlayerStates.Grappling)) {
            // unable to set grapple state, so don't enable it for this player
            return;
        }
        EnableGrapple(other.gameObject);
    }

    public void OnTriggerExit2D(Collider2D other) {
        if (other.isTrigger) {
            // Ignore trigger colliders - we only want physics colliders
            return;
        }

        // Get the other player
        if (!other.CompareTag(Tags.Player)) {
            //Debug.Log(config.PlayerNumber + " collider not other player");
            return;
        }

        if (GrappleManager.Instance.GrappleComplete) {
            // Grapple has been won by a player, stop checking for grapple updates
            return;
        }
        if (GrappleManager.Instance.GrappleInitiated) {
            // If initiated, do not cancel out the grapple
            return;
        }
        if (!_state.TryState(PlayerStates.Grappling)) {
            // unable to set grapple state, so don't disable it for this player
            return;
        }

        CancelGrapple();
    }
}