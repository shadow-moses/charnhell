﻿using System.Linq;

using UnityEngine;

/// <summary>
///     Player configuration data
/// </summary>
[ExecuteInEditMode]
public class PlayerConfig : MonoBehaviour {
    /// <summary>
    ///     The player's number
    /// </summary>
    public PlayerNumbers PlayerNumber = PlayerNumbers.P1;

    /// <summary>
    ///     The player's tint - white is normal sprite colour
    /// </summary>
    public Color PlayerColour = Color.white;

    /// <summary>
    ///     The layer to assign this player to
    /// </summary>
    public string LayerName = Layers.Player1;

    // Shared components
    public SpriteRenderer PlayerSpriteRenderer { get; private set; }
    public CircleCollider2D MovementCollider { get; private set; }
    public CircleCollider2D TriggerCollider { get; private set; }

    public Animator PlayerAnimator { get; private set; }
    public PlayerStateManager PlayerStateManager { get; private set; }
    public PlayerMovement PlayerMovement { get; private set; }
    public PlayerGrapple PlayerGrapple { get; private set; }
    public PlayerWeaponHandler PlayerWeaponHandler { get; private set; }
    public Rigidbody2D PlayerRigidbody { get; private set; }
    public AudioPool PlayerAudioPool { get; private set; }

    public void Awake() {
        // Set player's colour
        PlayerSpriteRenderer = GetComponent<SpriteRenderer>();
        PlayerSpriteRenderer.color = PlayerColour;

        // Assign layer malarky
        gameObject.layer = LayerMask.NameToLayer(LayerName);

        // Get shared components
        var colliders = GetComponents<CircleCollider2D>();
        MovementCollider = colliders.First(x => !x.isTrigger);
        TriggerCollider = colliders.First(x => x.isTrigger);

        PlayerAnimator = GetComponent<Animator>();
        PlayerStateManager = GetComponent<PlayerStateManager>();
        PlayerMovement = GetComponent<PlayerMovement>();
        PlayerGrapple = GetComponent<PlayerGrapple>();
        PlayerWeaponHandler = GetComponent<PlayerWeaponHandler>();

        PlayerRigidbody = GetComponent<Rigidbody2D>();

        if (PlayerNumber == PlayerNumbers.P1) {
            PlayerAudioPool = AudioPool.Player1;
        } else {
            PlayerAudioPool = AudioPool.Player2;
        }
    }
}