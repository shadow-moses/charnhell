﻿using UnityEngine;

public static class SpawnHelper {
    /// <summary>
    /// Returns a random location within the <see cref="spawnExtents"/> where an item can spawn
    /// </summary>
    /// <returns></returns>
    public static Vector2 GetArenaSpawnLocation(BoxCollider2D spawnExtents) {
        // Generate a random position inside the collider
        var randomX = Random.Range(spawnExtents.bounds.min.x, spawnExtents.bounds.max.x);
        var randomY = Random.Range(spawnExtents.bounds.min.y, spawnExtents.bounds.max.y);

        // TODO: Do a raycast / 2d spherecast to recursively check location
        var location = new Vector2(randomX, randomY);
        if (Physics2D.OverlapCircle(location, 0.15f, LayerMask.NameToLayer(Layers.Walls))) {
            // recurse
            location = GetArenaSpawnLocation(spawnExtents);
        }

        return location;
    }
}