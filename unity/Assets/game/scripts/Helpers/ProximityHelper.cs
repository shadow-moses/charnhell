﻿using UnityEngine;

public static class ProximityHelper {
    /// <summary>
    ///     Finds the nearest game object to the specified <see cref="nearestTo" /> position, at the <see cref="maxDistance" />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="nearestTo"></param>
    /// <param name="maxDistance"></param>
    /// <param name="searchList"></param>
    /// <returns></returns>
    public static GameObject FindNearest(Vector3 nearestTo, float maxDistance, GameObject[] searchList) {
        var nearestDistanceSqr = Mathf.Infinity;
        GameObject nearestObj = null;

        foreach (var thisObj in searchList) {
            // If this object has a renderer, use the center of this, other use transform
            var r = thisObj.GetComponent<Renderer>();
            var objectPos = r != null ? r.bounds.center : thisObj.transform.position;

            var distanceSqr = (objectPos - nearestTo).sqrMagnitude;

            if (!(distanceSqr < maxDistance) || !(distanceSqr < nearestDistanceSqr)) {
                continue;
            }

            nearestObj = thisObj;
            nearestDistanceSqr = distanceSqr;
        }

        return nearestObj;
    }
}