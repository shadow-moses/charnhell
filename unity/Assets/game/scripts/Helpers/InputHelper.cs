﻿public static class InputHelper {
    /// <summary>
    /// Returns an input identifier string for the specified player
    /// </summary>
    /// <param name="playerNumber"></param>
    /// <param name="axis"></param>
    /// <param name="suffix"></param>
    /// <returns></returns>
    public static string GetPlayerControl(PlayerNumbers playerNumber, string axis, string suffix = null) {
        var o = string.Concat("P", (int)playerNumber, "-", axis);
        if (!string.IsNullOrEmpty(suffix)) {
            o = string.Concat(o, "-", suffix);
        }
        return o;
    }

    /// <summary>
    /// Determines if the specified axis value is outside of the default deadzone, using the specified axis direction
    /// </summary>
    /// <param name="axisVal"></param>
    /// <param name="dir"></param>
    /// <returns></returns>
    public static bool OutOfDeadZone(float axisVal, AxisDir dir) {
        const float dz = 0.5f;
        switch (dir) {
            case AxisDir.Positive:
                return axisVal > dz;
            case AxisDir.Negative:
                return axisVal < -dz;
        }
        return false;
    }
}