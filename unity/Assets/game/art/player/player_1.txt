{"frames": {

"001_player_01_walk_s_01":
{
	"frame": {"x":0,"y":0,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"002_player_01_walk_s_02":
{
	"frame": {"x":27,"y":0,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"003_player_01_walk_s_03":
{
	"frame": {"x":54,"y":0,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"004_player_01_walk_s_04":
{
	"frame": {"x":81,"y":0,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"005_player_01_walk_n_01":
{
	"frame": {"x":108,"y":0,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"006_player_01_walk_n_02":
{
	"frame": {"x":135,"y":0,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"007_player_01_walk_n_03":
{
	"frame": {"x":162,"y":0,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"008_player_01_walk_n_04":
{
	"frame": {"x":189,"y":0,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"009_player_01_walk_e_01":
{
	"frame": {"x":216,"y":0,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"010_player_01_walk_e_02":
{
	"frame": {"x":0,"y":27,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"011_player_01_walk_e_03":
{
	"frame": {"x":27,"y":27,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"012_player_01_walk_e_04":
{
	"frame": {"x":54,"y":27,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"013_player_01_walk_w_01":
{
	"frame": {"x":81,"y":27,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"014_player_01_walk_w_02":
{
	"frame": {"x":108,"y":27,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"015_player_01_walk_w_03":
{
	"frame": {"x":135,"y":27,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"016_player_01_walk_w_04":
{
	"frame": {"x":162,"y":27,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"017_player_01_grapple_s_01":
{
	"frame": {"x":189,"y":27,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"018_player_01_grapple_s_02":
{
	"frame": {"x":216,"y":27,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"019_player_01_grapple_n_01":
{
	"frame": {"x":0,"y":54,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"020_player_01_grapple_n_02":
{
	"frame": {"x":27,"y":54,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"021_player_01_grapple_e_01":
{
	"frame": {"x":54,"y":54,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"022_player_01_grapple_e_02":
{
	"frame": {"x":81,"y":54,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"023_player_01_grapple_w_01":
{
	"frame": {"x":108,"y":54,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"024_player_01_grapple_w_02":
{
	"frame": {"x":135,"y":54,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"025_player_01_idle_01":
{
	"frame": {"x":162,"y":54,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"026_player_01_idle_02":
{
	"frame": {"x":189,"y":54,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"027_player_01_idle_03":
{
	"frame": {"x":216,"y":54,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"028_player_01_dash_s_01":
{
	"frame": {"x":0,"y":81,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"029_player_01_dash_n_01":
{
	"frame": {"x":27,"y":81,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"030_player_01_dash_e_01":
{
	"frame": {"x":54,"y":81,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"031_player_01_dash_w_01":
{
	"frame": {"x":81,"y":81,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"032_player_01_stunned_01":
{
	"frame": {"x":108,"y":81,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"033_player_01_stunned_02":
{
	"frame": {"x":135,"y":81,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"034_player_01_stunned_03":
{
	"frame": {"x":162,"y":81,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"035_player_01_dead_01":
{
	"frame": {"x":189,"y":81,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"036_player_01_dead_02":
{
	"frame": {"x":216,"y":81,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"037_player_01_dead_03":
{
	"frame": {"x":0,"y":108,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
},
"038_player_01_dead_04":
{
	"frame": {"x":27,"y":108,"w":27,"h":27},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":27,"h":27},
	"sourceSize": {"w":27,"h":27}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker ",
	"version": "1.0",
	"image": "player_1.png",
	"format": "RGBA8888",
	"size": {"w":256,"h":256},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:b7ecf7ab266b2085815578ec19e8731e:96fdaa9aeda4942a565b85308d552044:9ec0b37d65b95214b65cced49d970cf7$"
}
}
