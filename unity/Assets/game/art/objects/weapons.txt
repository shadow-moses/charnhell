{"frames": {

"001_weapon_skull.png":
{
	"frame": {"x":2,"y":2,"w":6,"h":5},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":6,"h":5},
	"sourceSize": {"w":6,"h":5}
},
"002_weapon_arm_01.png":
{
	"frame": {"x":10,"y":2,"w":6,"h":5},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":6,"h":5},
	"sourceSize": {"w":6,"h":5}
},
"003_weapon_arm_02.png":
{
	"frame": {"x":18,"y":2,"w":5,"h":6},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":5,"h":6},
	"sourceSize": {"w":5,"h":6}
},
"004_weapon_ribcage.png":
{
	"frame": {"x":25,"y":2,"w":6,"h":5},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":6,"h":5},
	"sourceSize": {"w":6,"h":5}
},
"005_weapon_leg_01.png":
{
	"frame": {"x":33,"y":2,"w":7,"h":3},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":7,"h":3},
	"sourceSize": {"w":7,"h":3}
},
"006_weapon_leg_02.png":
{
	"frame": {"x":42,"y":2,"w":6,"h":3},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":6,"h":3},
	"sourceSize": {"w":6,"h":3}
},
"007_weapon_spear.png":
{
	"frame": {"x":50,"y":2,"w":19,"h":5},
	"rotated": false,
	"trimmed": false,
	"spriteSourceSize": {"x":0,"y":0,"w":19,"h":5},
	"sourceSize": {"w":19,"h":5}
}},
"meta": {
	"app": "http://www.codeandweb.com/texturepacker ",
	"version": "1.0",
	"image": "weapons.png",
	"format": "RGBA8888",
	"size": {"w":256,"h":256},
	"scale": "1",
	"smartupdate": "$TexturePacker:SmartUpdate:09a88370a2273e962ae013e3ce26d0d4:cf7855e2a7226a8cf502b782c9a772a0:e2d9ef5832756b0e784087f40985ee07$"
}
}
